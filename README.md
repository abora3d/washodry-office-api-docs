# WashODry Office App - API Doc

## Base Url

- **Sandbox** : `http://api.office.sandbox.washodry.com/`
- **Production** : `http://api.office.washodry.com/`

--------------------------------------------------------------------------------

## Apis

API                                         | Method | EndPoint
------------------------------------------- | :----: | --------------------------------------
[Login](API/Auth.md)                           |  GET   | `/v1/auth/login`
[Logout](API/Auth.md)                          | DELETE | `/v1/auth/logout`
[Orders Documentaion](API/Orders/README.md)     |   --   | --
[Read All Notifications](API/Notifications.md) |  GET   | `/v1/notifications/all`
[Read One Notifications](API/Notifications.md) |  GET   | `/v1/notifications/{:notification_id}`
