# Notifications Guide

1. While loading the appliction, the app should call the configuration API to get Notifications settions like pusher sbuscribe key and channels perfix
2. Those keys should be hold in the memory
3. While the User Login to the application, app should subscribe to notification channel at the time.
   - Use `keys.pusher` from config api to connect to pusher
   - Use `notification_channels.private_channel_prefix` + `notification_channel` from login api and combine the two values togather to generate the channel
   - Use `notification_channels.private_channel_events` to subscribe to all the events
4. on logout or session expired, the app should unsubscribe from the channel.

## Example
- Config api : keys.pusher = xxxxx123456
- Config api : `notification_channels.private_channel_prefix` = "development_v1_emp_"
- Login  api : `notification_channel` = "emp-2"
- Config api : `notification_channels.customer_private_channel_events` = `orders`, `payments`
- Channel will look = "development_v1_emp_emp-2"


```js
var pusher = new Pusher('xxxxx123456');
var channel = pusher.subscribe('development_v1_emp_emp-2');
channel.bind('orders', function(data) {
  alert('do something with order notifications');
});
channel.bind('payments', function(data) {
  alert('do another thing with payments notifications');
});
```


----
