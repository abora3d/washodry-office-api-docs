# Notificaitons


## Notifications List

Get All Notifications History

## API:

**GET:** `/v1/notifications`

## Parameters

Parameter | Descriptions | Required | Validation | Example
--------- | ------------ | :------: | ---------- | -------
**page**  | Page Number  | Optional | Integer    | 2

### Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/notifications?page=1&access_token=dccd06fe9c12943153557873c5e6b64b'
```

--------------------------------------------------------------------------------

## Response

### Success

```json
{
  "status": 200,
  "message": "All Notifications",
  "data": {
    "not_seend": 36,
    "notifications": [
      {
        "id": 271,
        "push_type": 101,
        "body": {
          "push_type": 101,
          "title": "New Order in Jersey City",
          "message": "New Order in Jersey City",
          "order_id": 57
        },
        "seen": true,
        "created_at": "2016-10-31T08:02:16.000Z"
      },
      {
        "id": 265,
        "push_type": 101,
        "body": {
          "push_type": 101,
          "title": "New Order in Jersey City",
          "message": "New Order in Jersey City",
          "order_id": 56
        },
        "seen": false,
        "created_at": "2016-10-31T07:59:25.000Z"
      }
    ],
    "paging": {
      "current_page": "1",
      "page_records": 10,
      "total_records": 37,
      "total_pages": 4,
      "next_page": 2,
      "previous_page": null
    }
  }
}
```

#### Explain

data contain:
- **not_seend**: The count of unseen Notifications
- **notifications**: An Array of notifications history
    - **id**: notification id, will be used only to mark notification as seen
    - **push_type**: not used for now, just ignore it
    - **body**: the body of notification package that been sent
        - **push_type**: not used for now, just ignore it
        - **title**: Notification Title
        - **message**: Notification message
        - **order_id**: Order ID, will be used to navigate to the order page
    - **seen**: true for seen notifications, false for new notifications
    - **created_at**: the time of firing this notifications

--------------------------------------------------------------------------------

## Read All Notifications

Mark All Notificaitons as Read

## API:

**PUT:** `/v1/notifications/all`

### Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/notifications/all?access_token=dccd06fe9c12943153557873c5e6b64b'
```


## Response

### Success

```json
{
    "status": 200,
    "message": "All notifications marked as seen",
    "data": null
}
```

--------------------------------------------------------------------------------

## Read One Notifications

Mark single notifications as read

## API:

**GET:** `/v1/notifications/{:notification_id}`

### Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/notifications/{:notification_id}?access_token=dccd06fe9c12943153557873c5e6b64b'
```


## Response

### Success

```json
{
    "status": 200,
    "message": "Data Found",
    "data": {
        "notification": {
            "id": 259,
            "push_type": 101,
            "body": {
                "push_type": 101,
                "title": "New Order in Jersey City",
                "message": "New Order in Jersey City",
                "order_id": 55
            },
            "seen": true,
            "created_at": "2016-10-31T07:58:30.000Z"
        }
    }
}
```

#### Explain

data will contain the single notification record after update, it will be useful to instantly update the record in the consumer device:

- **notification**:
    - **id**: notification id, will be used only to mark notification as seen
    - **push_type**: not used for now, just ignore it
    - **body**: the body of notification package that been sent
        - **push_type**: not used for now, just ignore it
        - **title**: Notification Title
        - **message**: Notification message
        - **order_id**: Order ID, will be used to navigate to the order page
    - **seen**: true for seen notifications, false for new notifications
    - **created_at**: the time of firing this notifications
