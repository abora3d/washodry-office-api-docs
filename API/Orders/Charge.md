# Charge

Charge the Order Bill, Only Available for Admins

Order can be charged if `order.status_code` > **5**

--------------------------------------------------------------------------------

# API:

**GET:** `/v1/orders/61/charge`

## Curl Example

```
curl -X POST 'http://api.office.sandbox.washodry.com/v1/orders/61/charge&access_token=ef2b9be8196686fedfddc46f8524fc14'
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
    "status": 201,
    "message": "Charged Sucsessfully",
    "data": {
        "order": {}
    }
}
```

### Errors

```json
{
    "status": 400,
    "error": "You can`t charge the order before complete the laundry",
    "data": null
}
```
