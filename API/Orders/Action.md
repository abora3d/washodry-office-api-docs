# Action

Action API is responsible to update the status of the order.

--------------------------------------------------------------------------------

# API:

**GET:** `/v1/orders/{:order_id}/action`

# Parameters

Parameter | Descriptions | Required | Validation | Example
--------- | ------------ | :------: | ---------- | -------
**status**  | the new stauts of the order , Status codes explained in the [Orders Index](Index)  | Required | Integer    | 2

## Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/orders/60/action?status=6&access_token=3fbad78b348b51a7b18625b326540b56' -X POST
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
    "status": 200,
    "message": "Order 60 status changed successfully to On way to drop-off",
    "data": {
        "order": {
            "id": 60,
            "status": "On way to drop-off",
            "status_code": 6
        }
    }
}
```

### Explain

in data response there will be an `order` object that have:
- **id** : the Order ID, will be used to navigate to the order page
- **status** : the status label of the order
- **status_code** : the status code of the order

this response will be helpeful to update the order status in the consumer device WITHOUT hitting Show Order API again.
