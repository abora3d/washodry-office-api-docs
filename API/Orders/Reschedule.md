# Reschedule

Lorem ipsum dolor sit amet, consectetur adipisicing elit.

--------------------------------------------------------------------------------

# API:

**PUT:** `/v1/orders/64/reschedule`

# Parameters

Parameter         | Descriptions         | Required | Validation             | Example
----------------- | -------------------- | :------: | ---------------------- | -------
**pickup_date**   | Pickup Date          | Required | Date (yyyy-mm-dd)      | 2017-02-02
**pickup_time**   | Pickup Time Range    | Required | Accepted Timings (Key) | 08-10
**delivery_date** | Delivery Date        | Required | Date (yyyy-mm-dd), (Should be >= pickup_date) | 2017-02-01
**delivery_time** | Delivery Time Range  | Required | Accepted Timings (Key) | 20-22


## Accepted Timings

Key   | Appear to user (Value)
----- | --------------
08-10 | 08AM - 10AM
10-12 | 10AM - 12PM
12-14 | 12PM - 02PM
14-16 | 02PM - 04PM
16-18 | 04PM - 06PM
18-20 | 06PM - 08PM
20-22 | 08PM - 10PM
22-24 | 10PM - 12AM

## Curl Example

```
curl -X PUT  'http://api.office.sandbox.washodry.com/v1/orders/64/reschedule?&access_token=3f444c06afd4eb762951e765593aad9b&delivery_date=2017-02-01&delivery_time=16-18&pickup_date=2017-02-02&pickup_time=16-18'
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
    "status": 200,
    "message": "Order rescuduled successfully",
    "data": null
}
```

---

## Errors


```json
{
    "status": 400,
    "error": "Error in rescuduling the order",
    "data": null
}
```
