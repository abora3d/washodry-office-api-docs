# Show Single Order

Show full details about one order

--------------------------------------------------------------------------------

# API:

**GET:** `/v1/orders/{:order_id}`

# Parameters

Parameter    | Descriptions        |      Required       | Validation | Example
------------ | ------------------- | :-----------------: | ---------- | -------
**page**     | Page Number         |      Optional       | Integer    | 2
**order_id** | The ID of the Order | Required In the URL | Integer    | 2123

## Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/orders/67?access_token=dccd06fe9c12943153557873c5e6b64b'
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
  "status": 200,
  "message": "Show Order",
  "data": {
    "order": {
      "id": 67,
      "status": "Pending",
      "status_code": 0,
      "zone_id": 2,
      "zone_name": "South Jersey City",
      "pickup_date": "2017-01-03",
      "pickup_time": "14-16",
      "delivery_date": "2017-01-04",
      "delivery_time": "14-16",
      "order_date": "2017-01-02T00:28:43.000Z",
      "payment_id": null,
      "customer": {
        "name": "Demo1",
        "email": "a@a.com",
        "phone_number": "1-234-567-890"
      },
      "total_items": 13,
      "total_weight": "0.0",
      "subtotal": "65.9",
      "vat_value": "7.0",
      "vat_amount": "4.61",
      "delivery_charges": "0.0",
      "total": "70.51",
      "dry_instructions": "",
      "wash_instructions": "",
      "press_instructions": "",
      "pickup_address": {
        "title": "Home",
        "address": "Highway Route, New Lisbon, NJ\n, New Lisbon, New Jersey , 07030",
        "phone_number": "96325874118"
      },
      "delivery_address": {
        "title": "home",
        "address": "agsjdbakdbd, jersey, states, 07030",
        "phone_number": ".8180421582"
      },
      "items": [
        {
          "product_id": 7,
          "product_name": "Suit 2 piece ",
          "product_unit": "Pcs.",
          "product_category": 1,
          "quantity": 1,
          "price": "6.00"
        },
        {
          "product_id": 19,
          "product_name": "Tie",
          "product_unit": "Pcs.",
          "product_category": 1,
          "quantity": 10,
          "price": "2.99"
        },
        {
          "product_id": 20,
          "product_name": "Sherwani  ( Indian Wedding Dress) ",
          "product_unit": "Pcs.",
          "product_category": 1,
          "quantity": 2,
          "price": "15.00"
        }
      ],
      "payable": true
    }
  }
}
```

### Explain

Order details will be under `data.order`, it will be an Object:

- **id** : the Order ID, will be used to navigate to the order page
- **status** : the status label of the order
- **status_code** : the status code of the order
- **zone_id** : the zone id, Used On web to navigate to the zone details
- **zone_name** : the zone name
- **pickup_date** : Date of Pickup, `yyyy-mm-dd`
- **pickup_time** : Pickup Time window in 24 format
- **delivery_date** : Date of delivery/drop-off, `yyyy-mm-dd`
- **delivery_time** : delivery/drop-off Time window in 24 format
- **order_date** : The Date and time of create the order by customer , in UTC
- **payment_id** : Payment ID, if order has been charged the ID will show , otherwise `NULL`
- **customer** : `OBJECT`
  - **name** : Name Of Customer
  - **email** : Email of Customer
  - **phone_number** : Contach Number Of Customer
- **total_items** : The Total Number of Items that not measured by weight
- **total_weight** : The Total Weight of Items that measured by weight
- **subtotal** : The Sub Totla of the Bill
- **vat_value** : The Tax Value Like (5%)
- **vat_amount** : The Tax Amount that applied in Subtotal
- **delivery_charges** : Delivery Charges of the Order
- **total** : Total Bill
- **dry_instructions** : the Dry Instructions if exist, otherwise `NULL`
- **wash_instructions** : the Wash Instructions if exist, otherwise `NULL`
- **press_instructions** : the Press Instructions if exist, otherwise `NULL`
- **pickup_address** : `OBJECT`
  - **title** : Address Title (Home/Office ...)
  - **address** : Full Address
  - **phone_number** : Different Contact of the process if exist, otherwise `Null`
- **delivery_address** : `OBJECT`
  - **title** : Address Title (Home/Office ...)
  - **address** : Full Address
  - **phone_number** : Different Contact of the process if exist, otherwise `Null`
- **items** : `Array of Objects`
  - **product_id** : Product ID
  - **product_name** : Product Name
  - **product_unit** : The Unit Of measuring
  - **product_category** : Product Category ID
  - **quantity** : The Quantity of the Item
  - **price** : The Price of Single Item at the time of making the order
- **payable** : `TRUE` if the order can be charged now, `FALSE` if the order can't be charged or already charged, (used to enable or disable charge button)
