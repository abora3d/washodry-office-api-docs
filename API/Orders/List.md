# List Orders

Show All the Orders in the Drivers Zone. new and old.

--------------------------------------------------------------------------------

## API:

**GET:** `/v1/orders`

## Parameters

Parameter | Descriptions | Required | Validation | Example
--------- | ------------ | :------: | ---------- | -------
**page**  | Page Number  | Optional | Integer    | 2

### Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/orders?page=1&access_token=dccd06fe9c12943153557873c5e6b64b'
```

--------------------------------------------------------------------------------

## Response

### Success

```json
{
  "status": 200,
  "message": "Orders History",
  "data": {
    "orders": [
      {
        "id": 89,
        "status": "Pending",
        "status_code": 0,
        "zone_id": 2,
        "zone_name": "South Jersey City",
        "pickup_date": "2017-01-05",
        "pickup_time": "16-18",
        "delivery_date": "2017-01-06",
        "delivery_time": "16-18",
        "order_date": "2017-01-05T01:34:52.000Z",
        "payment_id": null,
        "customer_name": "Demo1",
        "address": "somewhere, 07306",
        "zipcode": "07306"
      },
      {
        "id": 88,
        "status": "Pending",
        "status_code": 0,
        "zone_id": 2,
        "zone_name": "South Jersey City",
        "pickup_date": "2017-01-05",
        "pickup_time": "16-18",
        "delivery_date": "2017-01-06",
        "delivery_time": "16-18",
        "order_date": "2017-01-05T01:29:47.000Z",
        "payment_id": "ST-0508-XXXXX",
        "customer_name": "Demo1",
        "address": "somewhere, 07306",
        "zipcode": "07306"
      }
    ],
    "paging": {
      "current_page": 1,
      "page_records": 10,
      "total_records": 28,
      "total_pages": 3,
      "next_page": 2,
      "previous_page": null
    }
  }
}
```

#### Explain

Orders will be under `data.orders`, it will be an Array of Objects:

- **id** : the Order ID, will be used to navigate to the order page
- **status** : the status label of the order
- **status_code** : the status code of the order
- **zone_id** : the zone id, Used On web to navigate to the zone details
- **zone_name** : the zone name
- **pickup_date** : Date of Pickup, `yyyy-mm-dd`
- **pickup_time** : Pickup Time window in 24 format
- **delivery_date** : Date of delivery/drop-off, `yyyy-mm-dd`
- **delivery_time** : delivery/drop-off Time window in 24 format
- **order_date** : The Date and time of create the order by customer , in UTC
- **payment_id** : Payment ID, if order has been charged the ID will show , otherwise `NULL`
- **customer_name** : Customer Name
- **address** : Customer Pickup full address
- **zipcode** : Customre Pickup zipcode
