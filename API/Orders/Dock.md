# Orders Dock

Show The New Orders and Orders in Progress in Drivers Zone.

--------------------------------------------------------------------------------

# API:

**GET:** `/v1/orders/dock`

# Parameters

Parameter | Descriptions | Required | Validation | Example
--------- | ------------ | :------: | ---------- | -------
**page**  | Page Number  | Optional | Integer    | 2
**type**  | Show list of active orders by type: <br>0: all the types <br> 1: pending<br> 2: in laundry<br> 3: ready<br>4: in process  | Optional (default: 0) | Integer    | 1

## Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/orders/dock?page=1&access_token=dccd06fe9c12943153557873c5e6b64b'
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
  "status": 200,
  "message": "Orders in Dock",
  "data": {
    "orders": [
      {
        "id": 19,
        "status": "Ready for drop-off",
        "status_code": 5,
        "zone_id": 5,
        "zone_name": "Jersey Heights ",
        "pickup_date": "2016-08-20",
        "pickup_time": "08-10",
        "delivery_date": "2016-08-30",
        "delivery_time": "20-22",
        "order_date": "2016-08-18T19:18:01.000Z",
        "payment_id": null,
        "customer_name": "Traci McMurray",
        "address": "somewhere, 07306",
        "zipcode": "07306"

      },
      {
        "id": 48,
        "status": "Pickeded up",
        "status_code": 3,
        "zone_id": 2,
        "zone_name": "South Jersey City",
        "pickup_date": "2016-12-19",
        "pickup_time": "16-18",
        "delivery_date": "2016-12-20",
        "delivery_time": "16-18",
        "order_date": "2016-12-19T13:49:48.000Z",
        "payment_id": null,
        "customer_name": "Demo1",
        "address": "somewhere, 07306",
        "zipcode": "07306"
      }
    ],
    "paging": {
      "current_page": 1,
      "page_records": 25,
      "total_records": 25,
      "total_pages": 1,
      "next_page": null,
      "previous_page": null
    }
  }
}
```

### Explain

Orders will be under `data.orders`, it will be an Array of Objects:

- **id** : the Order ID, will be used to navigate to the order page
- **status** : the status label of the order
- **status_code** : the status code of the order
- **zone_id** : the zone id, Used On web to navigate to the zone details
- **zone_name** : the zone name
- **pickup_date** : Date of Pickup, `yyyy-mm-dd`
- **pickup_time** : Pickup Time window in 24 format
- **delivery_date** : Date of delivery/drop-off, `yyyy-mm-dd`
- **delivery_time** : delivery/drop-off Time window in 24 format
- **order_date** : The Date and time of create the order by customer , in UTC
- **payment_id** : Payment ID, if order has been charged the ID will show , otherwise `NULL`
- **customer_name** : Customer Names
- **address** : Customer Pickup full address
- **zipcode** : Customre Pickup zipcode
