# Show Bill

In order to build the form of updating the order you need to hit the Bill API,
which is responsible to provide the data of all products of the order zone, with the
order's quantity and bill detail.

--------------------------------------------------------------------------------

# API:

**GET:** `/v1/orders/{:order_id}/bill`

# Parameters

Parameter    | Descriptions        |      Required       | Validation | Example
------------ | ------------------- | :-----------------: | ---------- | -------
**order_id** | The ID of the Order | Required In the URL | Integer    | 2123

## Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/orders/1/bill?access_token=ecd26a81399d28e08609746e52ce8a2c'
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
  "status": 200,
  "message": "zone products with order details",
  "data": {
    "items": [
      {
        "product_id": 1,
        "product_name": "Regular Laundry",
        "product_unit": "LB",
        "product_category": 2,
        "quantity": 11,
        "price": "1.60"
      },
      {
        "product_id": 2,
        "product_name": "White Clothes",
        "product_unit": "LB",
        "product_category": 2,
        "quantity": 0,
        "price": "1.50"
      }
    ],
    "bill": {
      "total_items": 5,
      "total_weight": "11.0",
      "subtotal": "68.58",
      "vat_value": "7.0",
      "vat_amount": "4.8",
      "delivery_charges": "0.0",
      "total": "73.38"
    }
  }
}
```

### Explain

There are 2 components in the response of this api, `items` and `bill`

- **items**: An array of all items/products of the orders zone
    - **product_id**: Product Id
    - **product_name**: Product Name
    - **product_unit**: Product Unit
    - **product_category**: Product Category (1: Dry Clean, 2: Wash & Fold, 3: Wash & Press)
    - **quantity**: The quantity of the product in the order, 0 if item/product not in the order
    - **price**: The Price of Single Item at the time of making the order

- **bill**:
    - **total_items**: The Total Number of Items that not measured by weight
    - **total_weight**: The Total Weight of Items that measured by weight
    - **subtotal** : The Sub Totla of the Bill
    - **vat_value** : The Tax Value Like (5%)
    - **vat_amount** : The Tax Amount that applied in Subtotal
    - **delivery_charges** : Delivery Charges of the Order
    - **total** : Total Bill
