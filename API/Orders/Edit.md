# Edit Order

To change Order details, Items and Quantites, you need to hit this api.

You will pass an `Items` Hash with this request that contain Products ids as Keys and The Quantites as Values that been returned or modified from [Bill Api](Bill).

Its better to remove an item from the order just set the value to 0, to add different item just add a quantity to it.

--------------------------------------------------------------------------------

# API:

**PUT:** `/v1/orders/{:order_id}`

# Parameters

Parameter | Descriptions | Required | Validation | Example
--------- | ------------ | :------: | ---------- | -------
**items[product_id]** | item is a Hash that have the product_id as key and quantity as value | Required | Hash | items[1]:5<br>items[2]:1<br>items[3]:0

## Curl Example

```
curl -g 'http://api.office.sandbox.washodry.com/v1/orders/60?access_token=3fbad78b348b51a7b18625b326540b56&items[1]=1&items[2]=5&items[3]=2' -X PUT
```

--------------------------------------------------------------------------------

# Response

## Success

```json
{
    "status": 200,
    "message": "Bill Updated Successfully",
    "data": null
}
```

---


## Error

If Order not matching minimum amount or weight?

```json
{
    "status": 400,
    "error": "Order dont match the minimum Order conditions",
    "data": null
}
```
