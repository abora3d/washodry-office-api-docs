# Orders APIs

## Apis

API                            | Desc                                                     | Method | EndPoint | Screen
------------------------------ | -------------------------------------------------------- | :----: | ----------------------------------- | :-:
[Orders List](List.md)            | List All Orders in Driver Zone                           |  GET   | `/v1/orders` | 1.5
[Orders Dock](Dock.md)            | List Orders not completed in deiver zone                 |  GET   | `/v1/orders/dock` | 1.1
[Show Order](Show.md)             | Show Single Order Details                                |  GET   | `/v1/orders/{:order_id}` | 2.1 , 2.2.x
[Show Order Items](Bill.md)       | Show full list of zone items and order Quantity for edit |  GET   | `/v1/orders/{:order_id}/bill` | 2.4
[Edit Order](Edit.md)             | Update Order Items and Quantity                          |  PUT   | `/v1/orders/{:order_id}` | 2.4 ( Update Buttom )
[Change Status](Action.md)        | Change the Status of the Order                           |  POST  | `/v1/orders/{:order_id}/action` | 2.1 ( Next Action Buttom )
[Reschedule Order](Reschedule.md) | Change the date and time of Pickup and Drop-off          |  POST  | `/v1/orders/{:order_id}/reschedule` | 2.3 ( Update Buttom )
[Charge Order](Charge.md)         | Charge The Order $$                                      |  POST  | `/v1/orders/{:order_id}/charge` | Only for Admin

--------------------------------------------------------------------------------

## Standards

### Order Status

ID | Status             | Explain | Label Color
-- | ------------------ | ---------------------------------------------------------------------------------- | :--:
0  | Pending            | The Order is placed by the customer, and waiting for our action | Grey (#d2d6de)
1  | Confirmed          | The Order is confirmed by our employees | Dark Blue (#3c8dbc)
2  | Pickup-in-process  | Our Driver went out to collect the order from the customer | Dark Blue (#3c8dbc)
3  | Pickeded up        | Our Driver collected the Order | Dark Blue (#3c8dbc)
4  | Laundering         | The Order dropped in Laundry Mart for the laundry | Light Blue (#00c0ef)
5  | Ready for drop-off | Laundry Mart Completed the laundry, and waiting to be collected | Light Blue (#00c0ef)
6  | On way to drop-off | Our Driver Collected the Order from the Mart and heading to Customer Drop Location | Light Blue (#00c0ef)
7  | Completed          | The Order is Deliveried to Customer, Completed and Charged Successfully | Green (#00a65a)
8  | Canceled           | The Order Canceled | Yellow (#d2d6de)

--------------------------------------------------------------------------------

### Pagination

For listing a lard number of orders, Pagination is good option to get small package of data for pages, by default, evey page will show **10** Records per page except docks will show 100.

#### Parameters:

Parameter | Descriptions                    | Example
--------- | ------------------------------- | -------
limit     | the limit of records per page   | 2
page      | the number of the required page | 2

#### package

the pagination details came under `data.paging` like the example

```json
{
    "status": 200,
    "message": "Orders History",
    "data": {
        "orders": [...],
        "paging": {
            "current_page": 1,
            "page_records": 10,
            "total_records": 54,
            "total_pages": 6,
            "next_page": 2,
            "previous_page": null
        }
    }
}
```

**Explain**:

- **current_page** : the value of the current page
- **page_records** : the limit value of records per page
- **total_records** : the total records
- **total_pages** : total number of pages
- **next_page** : next page number if exist , otherwise is `NULL`
- **previous_page** : previous page number if exist , otherwise is `NULL`

--------------------------------------------------------------------------------

### Products

#### Product Unit

ID   | UNIT
---- | -----
Pcs. | Piece
LB   | LB

#### Product Categories

ID | Category
-- | ------------
1  | Dry Clean
2  | Wash & Fold
3  | Wash & Press
