# Authentication

--------------------------------------------------------------------------------

For Office App Auth we use an access token that will be generated from server after successfull login

This token should provided on every request by one of two ways:

**By Link Parameter**

`/v1/orders?access_token={token}` ( Recommended for debugging )

**Provided on Request Header**

`Authorization: Bearer {token}` ( Recommended for Production )

**Life of token**

**Provided in the response in UTC timezone**.

--------------------------------------------------------------------------------

## Login

Login and generate access token.

**POST** : `/v1/auth/login`

Parameter    | Descriptions           | Required | Validation               | Example
------------ | ---------------------- | :------: | ------------------------ | ------------------
**login**    | User Email Or UserName | required | Email format             | example@domain.com
**password** | User Password          | required | length(min: 8, max: 255) | password

**Successful Response Example**: HTTP Status 200<br>

```json
{
    "status": 200,
    "message": "Successfully login",
    "data": {
        "user": {
            "first_name": "First Name",
            "last_name": "Last Name",
            "email": "a@a.com",
            "phone_number": "xxxxxx",
            "avatar": "http://placehold.it/350x350.jpg"
        },
        "auth": {
            "access_token": "7ae3b8e5de11db765bdb07df758ba0e3",
            "access_role": "admin",
            "access_token_expiration": "2017-01-10T17:35:24.226Z"
        },
        "zones": [1, 4, 3],
        "notification_channel": "emp-2"
    }
}
```

### Response Data Contains:

- **user**:

  - **first_name**: User First Name
  - **last_name**: User Last Name
  - **email**: User Email
  - **phone_number**: User Phone Number
  - **avatar**: The User's picture

- **auth**:

  - **access_role**: either ['admin' or 'delivery'], based on this value, menus and options will be shown/hidden/disabled or enabled
  - **access_token**: the token will be used other requests
  - **access_token_expiration**: time of expiring the token in UTC

- **zones**: the ids of zones of this user (might be used in pushin notifications based on zones channels)

- **notification_channel**: the main use notifications channel

--------------------------------------------------------------------------------

## Logout

Delete user session

**DELETE** : `/v1/auth/logout`

```json
{
    "status": 200,
    "message": "logout successfully",
    "data": null
}
```
