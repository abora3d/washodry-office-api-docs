# Configurations

Get main app configurations like 3rd party keys dynamically and etc.

This API should be hitted everytime the app is loaded. to get the correct keys of
the enviroment.

## API:

**GET:** `/v1/config`

### Curl Example

```
curl 'http://api.office.sandbox.washodry.com/v1/config'
```

--------------------------------------------------------------------------------

## Response

### Success


```json
{
  "status": 200,
  "message": "Data Found",
  "data": {
    "keys": {
      "pusher": "7d66bd30a76299e577fd"
    },
    "notification_channels": {
      "private_channel_prefix": "development_v1_emp_",
      "private_channel_events": [
        "orders"
      ]
    }
  }
}
```

### Explain

Data Contain :

- **keys** : A Hash that contain 3rd party services keys
    - **pusher**: The key of connecting to pusher
- **notification_channels**:
    - **private_channel_prefix**: the prefix of channel names
    - **private_channel_events**: array of channel events to subscribe

--------------------------------------------------------------------------------
